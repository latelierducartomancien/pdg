# L'Atelier du Cartomancien

Bienvenue sur le repo du projet _L'Atelier du Cartomancien_.

## Le projet en quelques mots

Le but de ce projet web est de créer un site proposant une génération
de cartes du monde pour notamment les jeux de rôle papier

## Lien du projet en production
Le site en production est disponible à l'adresse suivante:
<http://atelier-du-cartomancien.ch/>

## Technologies utilisés
- Langage backend: [Typescript](https://www.typescriptlang.org/)
- Backend: [Nest.js](https://nestjs.com/)
- Moteur de templating HTML: [handlebars](https://handlebarsjs.com/)
- Linter: [eslint](https://eslint.org/)
- Framework de test: [Jest](https://jestjs.io/)
## Installation et lancement du projet en local

Après avoir cloné le repo, on peut soit lancer le projet directement avec node
soit lancer dans un container avec docker

### Lancement avec Node

#### Pré-requis: 
- Avoir [Node.js](https://nodejs.org/en/) installé

#### Etapes:
2. Installer les dépendances à la racine du projet:
``npm install``
3. Lancer le projet en local avec ```npm run start``` 
ou en mode watch avec ```npm run start:dev``` 
4. Le site est disponible à l'adresse suivante: ``localhost:3000``

### Lancement avec Docker

#### Pré-requis:
- Avoir [Docker](https://www.docker.com/) installé

#### Etapes:
1. Installer les dépendances à la racine du projet:
   ``npm install``
2. Build le projet: 
    ``npm run build``
3. Créer l'image du serveur avec la commande suivante à la racine: 
```bash
docker build . -t "latelierducartomancien_node-server"
```
4. Démarrer un container du serveur avec la commande suivante:
````bash
docker run -e "PORT=3000" -p 3000:3000 latelierducartomancien_node-server
````
5. Le site est disponible à l'adresse suivante: ``localhost:3000``

## Wiki

Le wiki de ce repo contient notamment la structure du projet ainsi que
les instructions pour les contributions

