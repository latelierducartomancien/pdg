FROM node:14 AS development

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM node:14 AS production

COPY package*.json ./

RUN npm install --only=production

COPY --from=development /app/dist ./dist
COPY --from=development /app/public ./public
COPY --from=development /app/views ./views

EXPOSE 3000/tcp

CMD npm run start:prod
