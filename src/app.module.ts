import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {SettingsController} from './settings/settings.controller';
import {AppService} from './app.service';
import {ConfigurationService} from './configuration/configuration/configuration.service';
import {join} from 'path';
import {ServeStaticModule} from '@nestjs/serve-static';
import {MapController} from './map/map.controller';
import {MapService} from './map/map.service';

@Module({
  imports: [ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'public'),
  })],
  controllers: [AppController, SettingsController, MapController],
  providers: [AppService, ConfigurationService, MapService],
})
export class AppModule {
  static port: number;

  constructor(private readonly configurationService: ConfigurationService) {
    AppModule.port = this.configurationService.port;
  }
}
