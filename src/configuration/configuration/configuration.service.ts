import {Injectable} from '@nestjs/common';

@Injectable()
export class ConfigurationService {
  public port: number;

  constructor() {
    this.port = parseInt(process.env.PORT) || 3000;
  }
}
