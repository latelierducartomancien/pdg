import {Injectable} from '@nestjs/common';
import MapGenerator from '../mapGenerator/MapGenerator';
import {MapSettings} from '../Utils/MapSettings';

@Injectable()
export class MapService {
  /**
   * Génère une carte sous la forme d'un objet JSON à partir des réglages donnés.
   * @param {MapSettings} settings Les réglages utilisés pour générer la carte.
   * @return {string}
   */
  generate(settings : MapSettings): string {
    const mapGen = new MapGenerator(settings.mapWidth, settings.mapLength, settings.densityEs / 100, settings.densityR / 100, settings.nbCities);
    return JSON.stringify(mapGen.generate(10));
  }
}
