import {Controller, Get, Render, Req, Res} from '@nestjs/common';
import {Request, Response} from 'express';
import {MapService} from './map.service';
import {GlobalService} from '../global/global.service';

@Controller('map')
export class MapController {
  constructor(private readonly mapService: MapService) {
  }

  @Get('/')
  @Render('map')
  index() {
    return {
      title: 'Carte',
      titlePage: 'L\'Atelier du Cartomancien',
    };
  }

  @Get('/generate')
  generate(@Req() request : Request, @Res() response : Response) {
    const settings = GlobalService.getClientSetting(request.cookies['id']);
    if (settings == undefined) {
      // Pas de réglages associés à la clé, on redirige l'utilisateur vers la page de réglages.
      return response.redirect('/settings');
    }
    return response.send(this.mapService.generate(settings));
  }
}
