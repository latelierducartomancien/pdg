import {Vector2D} from '../Utils/Vector';

export class City {
  coords: Vector2D;
  name: string;

  constructor(name: string, coords: Vector2D) {
    this.name = name;
    this.coords = coords;
  }
}
