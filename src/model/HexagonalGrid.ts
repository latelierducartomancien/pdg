import {Cell} from './Cell';
import {River} from './River';
import {City} from './City';

/**
 * Grille hexagonale stockant les données de la carte
 */
export class HexagonalGrid {
  cellWidth: number;
  cellHeight: number;
  cells: Cell[][];
  rivers: River[];
  cities: City[];

  constructor() {
    this.cellHeight = 0;
    this.cellWidth = 0;
    this.cells = [];
    this.rivers = [];
    this.cities = [];
  }
}
