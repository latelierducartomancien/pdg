import {HexagonalGrid} from './HexagonalGrid';

/**
 * Carte contenant toutes les informations.
 */
export class GeneratedMap {
  grid: HexagonalGrid;
  width: number;
  height: number;
  seaLevel: number;
  riverDensity: number;

  constructor(height : number, width : number, seaLevel : number, riverDensity : number) {
    this.grid = new HexagonalGrid();
    this.height = height;
    this.width = width;
    this.seaLevel = seaLevel;
    this.riverDensity = riverDensity;
  }
}
