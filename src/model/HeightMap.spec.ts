import {HeightMap} from './HeightMap';
import {Vector2D} from '../Utils/Vector';
import {Cell} from './Cell';

describe('HeightMap class', () => {
  it('01 - test generated grid', () => {
    const nbX = 3;
    const nbY = 6;
    const hexGrid = new HeightMap(nbX, nbY);
    expect(hexGrid.cells.length).toStrictEqual(nbX);
    expect(hexGrid.cells[0].length).toStrictEqual(nbY);
  });
  it('02 - right offsets', () => {
    const hexGrid = new HeightMap(6, 6);
    for (let x = 0; x < hexGrid.cells.length; ++x) {
      for (let y = 0; y < hexGrid.cells[0].length; ++y) {
        const q = hexGrid.cells[x][y].position.x;
        const r = hexGrid.cells[x][y].position.y;
        expect(q).toStrictEqual(x - Math.floor(y / 2));
        expect(r).toStrictEqual(y);
      }
    }
  });
  it('03 - dimensions', () => {
    const X = 6;
    const Y = 6;
    const hexGrid = new HeightMap(X, Y);
    expect(hexGrid.xNum).toStrictEqual(X);
    expect(hexGrid.yNum).toStrictEqual(Y);
    expect(hexGrid.cells.length).toStrictEqual(X);
    expect(hexGrid.cells[X - 1].length).toStrictEqual(Y);
  });
  it('04 - getCellAtHexeCoord()', () => {
    const hexGrid = new HeightMap(6, 6);
    const grid = hexGrid.cells;
    // test ok parce que position est unique pour chaque cellule
    // on pointe une cell avec deux methodes et on vérifie
    // qu'elles sont les mêmes
    let cell = hexGrid.getCellAtHexCoord(new Vector2D(0, 0));
    expect(cell.position).toStrictEqual(grid[0][0].position);
    cell = hexGrid.getCellAtHexCoord(new Vector2D(1, 3));
    expect(cell.position).toStrictEqual(grid[2][3].position);
    cell = hexGrid.getCellAtHexCoord(new Vector2D(3, 5));
    expect(cell.position).toStrictEqual(grid[5][5].position);
  });
  it('05 - setCellAtHexeCoord()', () => {
    const hexGrid = new HeightMap(6, 6);
    hexGrid.setCellAtHexCoord(new Vector2D(-1, 3), new Cell(2, new Vector2D(-1, 3)));
    expect(hexGrid.getCellAtHexCoord(new Vector2D(-1, 3)).level)
        .toStrictEqual(2);
  });
});
