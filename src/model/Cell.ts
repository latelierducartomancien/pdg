import {Vector2D} from '../Utils/Vector';

/**
 * Cellule de la grille. Contient des positions et des informations.
 */
export class Cell {
  position: Vector2D;
  level: number;
  hasRiver: boolean;

  constructor(level : number, position : Vector2D, hasRiver = false) {
    this.hasRiver = hasRiver;
    this.position = position;
    this.level = level;
  }
}
