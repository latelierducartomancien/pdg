import {Vector2D} from '../Utils/Vector';

export class River {
  riverPoints: Vector2D[];

  constructor() {
    this.riverPoints = [];
  }
}
