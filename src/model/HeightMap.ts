import {Cell} from './Cell';
import {Vector2D} from '../Utils/Vector';

export class HeightMap {
  public cells: Cell[][];
  public xNum: number;
  public yNum: number;

  constructor(xNum: number, yNum: number) {
    this.cells = [];
    this.xNum = xNum;
    this.yNum = yNum;
    for (let x = 0; x < xNum; ++x) {
      this.cells[x] = [];
      for (let y = 0; y < yNum; ++y) {
        const pos = new Vector2D(x - Math.floor(y / 2), y);
        this.cells[x][y] = new Cell(0, pos, false);
      }
    }
  }

  /**
   * Converti les coordonnées d'un hexagone en coordonnées cartésiennes.
   * @param {Vector2D} hexCoord Les coordonnées de l'hexagone.
   * @return {Vector2D}
   */
  public static hexToCartesianCoord(hexCoord: Vector2D): Vector2D {
    const q = hexCoord.x;
    const r = hexCoord.y;
    const newX = q + Math.floor(r / 2);
    return new Vector2D(newX, r);
  }

  /**
   * Obtient l'hexagone aux coordonnées données.
   * @param {Vector2D} hexCoord Les coordonnées de l'hexagone.
   * @return {Cell}
   */
  public getCellAtHexCoord(hexCoord: Vector2D): Cell {
    const q = hexCoord.x;
    const r = hexCoord.y;
    const newX = q + Math.floor(r / 2);
    if (newX >= this.cells.length || newX < 0) {
      return undefined;
    } else if ( r >= this.cells[newX].length || r < 0) {
      return undefined;
    }
    return this.cells[newX][r];
  }

  /**
   * Stocke l'hexagone aux coordonnées données.
   * @param {Vector2D} hexCoord Les coordonnées de l'hexagone.
   * @param {Cell} cell L'hexagone à stocker.
   */
  public setCellAtHexCoord(hexCoord: Vector2D, cell: Cell): void {
    const q = hexCoord.x;
    const r = hexCoord.y;
    this.cells[q + Math.floor(r / 2)][r] = cell;
  }
}
