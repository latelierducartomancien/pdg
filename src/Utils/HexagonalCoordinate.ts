import {Vector2D} from './Vector';

/**
 * Classe représentant des coordonées hexagonales.
 */
export class HexCoord {
  q: number;
  r: number;

  constructor(q: number, r: number) {
    this.q = q;
    this.r = r;
  }

  /**
   * Converti des coordonnées cartésiennes en hexagonales.
   * @param {Vector2D} coord
   * @return {HexCoord}
   */
  static cartesianToHexCoord(coord: Vector2D): HexCoord {
    const q = coord.x - (coord.y - (coord.y & 1)) / 2;
    const r = coord.y;
    return new HexCoord(q, r);
  }

  private static HexDirections = [
    new HexCoord(+1, 0), new HexCoord(+1, -1), new HexCoord(0, -1),
    new HexCoord(-1, 0), new HexCoord(-1, +1), new HexCoord(0, +1),
  ];

  /**
   * Obtient les coordonnées hexagonales correspondant à une direction données.
   * @param {number} direction La direction dont on souhaite avoir les coordonnées.
   * @return {HexCoord}
   */
  private static hexDirection(direction: number): HexCoord {
    return HexCoord.HexDirections[direction];
  }

  /**
   * Additionne des coordonnées hexagonales.
   * @param {HexCoord} coord Les coordonnées à additionner.
   * @return {HexCoord}
   */
  add(coord: HexCoord): HexCoord {
    return new HexCoord(this.q + coord.q, this.r + coord.r);
  }

  /**
   * Obtient les coordonnées voisines dans une direction donnée.
   * @param {number} direction La direction dont on souhaite avoir les coordonnées.
   * @return {HexCoord}
   */
  neighbor(direction: number): HexCoord {
    return this.add(HexCoord.hexDirection(direction));
  }
}
