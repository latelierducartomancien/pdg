export class Vector2D {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  /**
   * Calcule le produit vectoriel de ce vecteur avec le vecteur donné.
   * @param {Vector2D} v Le vecteur avec lequel faire le produit.
   * @return {number} Le produit vectoriel.
   */
  dotProd(v: Vector2D): number {
    return this.x * v.x + this.y * v.y;
  }

  /**
   * Teste si deux vecteurs sont égaux.
   * @param {Vector2D} v Le vecteur avec lequel comparer.
   * @return {boolean} true si les deux vecteurs sont égaux, sinon false.
   */
  equals(v: Vector2D): boolean {
    return v.x === this.x && v.y === this.y;
  }

  /**
   * Génère un vecteur unitaire 2D aléatoire.
   * @return {Vector2D} Le vecteur généré.
   */
  static random(): Vector2D {
    const theta = Math.random() * 2 * Math.PI;
    return new Vector2D(Math.cos(theta), Math.sin(theta));
  }
}
