export class MapSettings {
  public mapLength: number;
  public mapWidth: number;
  public nbCities: number;
  public densityEs: number;
  public densityR: number;
  public expires: Date;

  constructor(mapLength: number, mapWidth: number, nbCities: number, densityEs: number, densityR: number, expires: Date) {
    this.mapLength = mapLength;
    this.mapWidth = mapWidth;
    this.nbCities = nbCities;
    this.densityEs = densityEs;
    this.densityR = densityR;
    this.expires = expires;
  }
}
