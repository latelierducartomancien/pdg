import {Controller, Get, Post, Redirect, Render, Req, Res} from '@nestjs/common';
import {Request, Response} from 'express';
import {v4 as uuidv4} from 'uuid';
import {GlobalService} from '../global/global.service';
import {MapSettings} from '../Utils/MapSettings';

@Controller('settings')
export class SettingsController {
  @Get()
  @Render('settings')
  index(@Req() request: Request, @Res({passthrough: true}) response: Response) {
    const id = request.cookies['id'];
    let options: MapSettings;
    if (id) {
      options = GlobalService.getClientSetting(id);
    } else {
      response.cookie('id', uuidv4(), {sameSite: 'lax'});
      options = new MapSettings(1000, 1000, 15, 50, 50, new Date(Date.now() + GlobalService.SETTINGS_DURATION_MIN * 60000));
    }

    return {
      title: 'Réglages',
      titlePage: 'Réglages de la carte',
      options,
    };
  }

  @Post()
  @Redirect('/map')
  storeSettings(@Req() request: Request) {
    let mapLength;
    let mapWidth;
    switch (request.body.map_dim) {
      case '2k':
        mapWidth = 1920;
        mapLength = 1080;
        break;
      case '4k':
        mapWidth = 3840;
        mapLength = 2160;
        break;
      case 'A4':
        mapWidth = 1754;
        mapLength = 1240;
        break;
      case 'A3':
        mapWidth = 2480;
        mapLength = 1754;
        break;
      default:
        mapWidth = 1000;
        mapLength = 1000;
        break;
    }
    const orientation = request.body.orientation;
    if (orientation == 2) {
      const tmp = mapWidth;
      mapWidth = mapLength;
      mapLength = tmp;
    }
    let nbCities  = parseInt(request.body.nb_cities);
    if (nbCities < 0 || nbCities > 30) {
      nbCities = 0;
    }
    let densityEs = parseInt(request.body.density_es);
    if (densityEs < 0) {
      densityEs = 0;
    } else if (densityEs > 100) {
      densityEs = 100;
    }
    let densityR  = parseInt(request.body.density_r);
    if (densityR < 0) {
      densityR = 0;
    } else if (densityR > 100) {
      densityR = 100;
    }
    const cookies = request.cookies;
    const id = cookies['id'];
    const expire = new Date( Date.now() + GlobalService.SETTINGS_DURATION_MIN * 60000);

    GlobalService.setClientSetting(id, new MapSettings(mapLength, mapWidth, nbCities, densityEs, densityR, expire));
  }
}
