import {Injectable} from '@nestjs/common';
import {MapSettings} from '../Utils/MapSettings';

@Injectable()
export class GlobalService {
  // La durée de vie minimale des réglages.
  public static SETTINGS_DURATION_MIN = 1;
  private static clientSettings = new Map<string, MapSettings>();

  /**
   * Récupère les réglages correspondant à la clé de l'utilisateur donnée.
   * @param {string} key La clé de l'utilisateur.
   * @return {MapSettings}
   */
  static getClientSetting(key : string): MapSettings {
    return this.clientSettings.get(key);
  }

  /**
   * Stocke les réglages correspondant à une clé donnée.
   * @param {string} key La clé de l'utilisateur.
   * @param {MapSettings} settings Les réglages à stocker.
   */
  static setClientSetting(key : string, settings: MapSettings ) {
    this.clientSettings.set(key, settings);
    this.cleanExpiredSettings();
  }

  /**
   * Supprime les réglages ayant expiré leur durée de vie.
   */
  static cleanExpiredSettings() {
    this.clientSettings.forEach((value, key, map) =>{
      if (value.expires.getTime() < Date.now()) {
        map.delete(key);
      }
    });
  }
}
