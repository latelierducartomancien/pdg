import HexagonalCellsGenerator, {ratio} from './HexagonalCellsGenerator';
import {Vector2D} from '../Utils/Vector';

describe('HexagonalCellsGenerator.ts', () => {
  describe('getCellCenter without shift', () => {
    it('01 - position (0,0) should return right coordinates', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const generator = new HexagonalCellsGenerator(cellWidth);
      expect(generator.getCellCenter(0, 0)).toStrictEqual(new Vector2D(cellWidth / 2, cellHeight / 2));
    });
    it('02 - position (1,0) should return right coordinates', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const generator = new HexagonalCellsGenerator(cellWidth);
      expect(generator.getCellCenter(1, 0)).toStrictEqual(new Vector2D(1.5 * cellWidth, cellHeight / 2));
    });
    it('03 - position (0,1) should return right coordinates', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const generator = new HexagonalCellsGenerator(cellWidth);
      expect(generator.getCellCenter(0, 1)).toStrictEqual(new Vector2D(cellWidth,  1.25 * cellHeight));
    });
  });
  describe('getCellCenter with shift', () => {
    it('04 - position', function() {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const shift = 10;
      const generator = new HexagonalCellsGenerator(cellWidth, shift, shift);
      expect(generator.getCellCenter(0, 0)).toStrictEqual(new Vector2D(cellWidth / 2 + shift, cellHeight / 2 + shift));
    });
    it('05 - position(0,0) should return coordinates(0,10)', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const shift = 10;
      const generator = new HexagonalCellsGenerator(cellWidth, -cellWidth/2, -cellHeight/2 + shift);
      expect(generator.getCellCenter(0, 0)).toStrictEqual(new Vector2D(0, shift));
    });
  });
});
