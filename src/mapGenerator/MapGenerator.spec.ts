import MapGenerator from './MapGenerator';
import {ratio} from './HexagonalCellsGenerator';

describe('MapGenerator.ts', () => {
  describe('generate grid', () => {
    it('01 - test generated grid', () => {
      const mapWidth = 1000;
      const mapHeight = 1000;
      const generator = new MapGenerator(mapWidth, mapHeight, 0, 0);
      const map = generator.generate(10);
      expect(map.grid.cellHeight).toStrictEqual(ratio(map.grid.cellWidth));
    });
    it('01 - grid dimensions should be >= map dimensions', () => {
      const cellWidth = 10;
      const cellHeight = ratio(cellWidth);
      const mapWidth = 1000;
      const mapHeight = 1000;
      const generator = new MapGenerator(mapWidth, mapHeight, 0, 0);
      const map = generator.generate(cellWidth);
      expect(map.grid.cellHeight).toStrictEqual(cellHeight);
      const nbX = Math.ceil(mapWidth / cellWidth) + 1;
      const nbY = Math.ceil(mapHeight / (cellHeight * 3/4.) ) + 1;
      const mapCellsWidth = nbX * cellWidth;
      const mapCellsHeight = nbY * (cellHeight * 3/4.);
      expect(mapCellsWidth).toBeGreaterThanOrEqual(mapWidth);
      expect(mapCellsHeight).toBeGreaterThanOrEqual(mapHeight);
    });
  });
});
