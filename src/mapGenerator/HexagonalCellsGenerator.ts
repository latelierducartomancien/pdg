import {Vector2D} from '../Utils/Vector';

/**
 * Calcule la hauteur d'une case en fonction de sa largeur.
 * @param {number} cellWidth La largeur de la case
 * @return {number}
 */
export function ratio(cellWidth: number): number {
  return 2 * cellWidth / Math.sqrt(3);
}

class HexagonalCellsGenerator {
  private readonly cellWidth: number;
  private readonly cellHeight: number;
  private readonly dw: number;
  private readonly dh: number;
  private readonly w0: number;
  private readonly h0: number;
  private readonly xShift: number;
  private readonly yShift: number;

  // Les cases ont une hauteur et une largeur.
  constructor(cellWidth : number, xShift = 0, yShift = 0) {
    this.cellWidth = cellWidth;
    this.cellHeight = ratio(cellWidth);
    this.xShift = xShift;
    this.yShift = yShift;
    this.dw = this.cellWidth / 2.;
    this.dh = this.cellHeight * 3 / 4.;
    this.w0 = this.cellWidth / 2.;
    this.h0 = this.cellHeight / 2.;
  }

  /**
   * Donne la position du centre de l'hexagone en fonction des coordonnées de la case dans la grille.
   * @param {number} x Le numéro de colonne de l'hexagone.
   * @param {number} y Le numéro de ligne de l'hexagone.
   * @return {Vector2D}
   */
  getCellCenter(x : number, y : number) : Vector2D {
    return new Vector2D(
        this.xShift + this.w0 + x * this.cellWidth + ((-1)**((y+1) % 2)+1)/2. * this.dw,
        this.yShift + this.h0 + y * this.dh);
  }
}

export default HexagonalCellsGenerator;
