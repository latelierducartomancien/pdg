import {PerlinGenerator} from './PerlinGenerator';

describe('PerlinGenerator.ts', () => {
  describe('generate grid', () => {
    it('01 - test generated grid', () => {
      const gen = new PerlinGenerator(10);
      const val = gen.generateHeight(0, 0);

      expect(val < 1).toBe(true);
      expect(val > -1).toBe(true);
      expect(val < 1).toBe(true);
      expect(val > -1).toBe(true);
    });
  });
});
