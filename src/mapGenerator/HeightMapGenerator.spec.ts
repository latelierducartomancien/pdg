import {ratio} from './HexagonalCellsGenerator';
import {HeightMapGenerator} from './HeightMapGenerator';

describe('HeightMapGenerator.ts', () => {
  describe('generate grid', () => {
    it('01 - test generated grid', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const gridSize = 1000;
      const nbX = Math.ceil(gridSize / cellWidth) + 1;
      const nbY = Math.ceil(gridSize / (cellHeight * 3/4.) ) + 1;
      const gen = new HeightMapGenerator(cellWidth, cellHeight);
      const grid = gen.generate(gridSize, gridSize);
      expect(grid.cells.length).toStrictEqual(nbX);
      expect(grid.cells[0].length).toStrictEqual(nbY);
    });
    it('02 - test size of generated grid', () => {
      const cellWidth = 40;
      const cellHeight = ratio(cellWidth);
      const gridSize = 1001;
      const nbX = Math.ceil(gridSize / cellWidth) + 1;
      const nbY = Math.ceil(gridSize / (cellHeight * 3/4.) ) + 1;
      const generator = new HeightMapGenerator(cellWidth, cellHeight);
      const grid = generator.generate(gridSize, gridSize);
      expect(grid.cells.length).toStrictEqual(nbX);
      expect(grid.cells[0].length).toStrictEqual(nbY);
    });
  });
});
