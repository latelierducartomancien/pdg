import {Cell} from '../model/Cell';
import HexagonalCellsGenerator from './HexagonalCellsGenerator';
import {PerlinGenerator} from './PerlinGenerator';
import {HeightMap} from '../model/HeightMap';

const MIN_OCT = 2; // Paramètre à modifier en fonction de la taille des hexagones
const MAX_OCT = 7; // Paramètre à modifier en fonction de la taille des hexagones
const PERSIST = 0.5;
const INITIAL_AMPLITUDE = 1;

export class HeightMapGenerator {
  private readonly cellWidth : number;
  private readonly cellHeight : number;

  constructor(cellWidth: number, cellHeight: number) {
    this.cellWidth = cellWidth;
    this.cellHeight = cellHeight;
  }

  /**
   * Génère une carte des hauteurs avec les dimensions données.
   * @param {number} mapWidth La largeur de la carte.
   * @param {number} mapHeight La hauteur de la carte.
   * @return {HeightMap}
   */
  generate(mapWidth: number, mapHeight: number): HeightMap {
    const xCellNb = Math.ceil(mapWidth / this.cellWidth) + 1;
    const yCellNb = Math.ceil(mapHeight / (this.cellHeight * 3/4.)) + 1;
    const grid = new HeightMap(xCellNb, yCellNb);
    const cellsGenerator = new HexagonalCellsGenerator(this.cellWidth, -this.cellWidth / 2, -this.cellHeight / 4);
    const noiseGenerators: {pg: PerlinGenerator, dim: number}[] = [];
    for (let o = MIN_OCT; o < MAX_OCT; ++o) {
      noiseGenerators.push({pg: new PerlinGenerator(2**o), dim: 2**o});
    }
    let min = MAX_OCT;
    let max = 0;
    for (let i = 0; i < xCellNb; ++i) {
      for (let j = 0; j < yCellNb; ++j) {
        const pos = cellsGenerator.getCellCenter(i, j);
        let amplitude = INITIAL_AMPLITUDE;
        let lvl = 0;
        for (let o = MIN_OCT; o < MAX_OCT; ++o) {
          // Rajout de 5x la largeur d'une cellule pour éviter une bande du même niveau à droite de la carte.
          const div = noiseGenerators[o - MIN_OCT].dim / Math.max(mapWidth + this.cellWidth * 5, mapHeight);
          lvl += (noiseGenerators[o - MIN_OCT].pg.generateHeight(pos.x * div, pos.y * div) / 2 + 0.5) * amplitude;
          amplitude *= PERSIST;
        }
        min = Math.min(lvl, min);
        max = Math.max(lvl, max);
        grid.cells[i][j] = new Cell(lvl, pos);
      }
    }
    // Normalise la hauteur.
    for (let i = 0; i < xCellNb; ++i) {
      for (let j = 0; j < yCellNb; ++j) {
        grid.cells[i][j].level = (grid.cells[i][j].level - min) / (max - min);
      }
    }
    return grid;
  }
}
