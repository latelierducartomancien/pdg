import {GeneratedMap} from '../model/GeneratedMap';
import {ratio} from './HexagonalCellsGenerator';
import {HeightMapGenerator} from './HeightMapGenerator';
import {RiverGenerator} from './RiverGenerator';
import {CityGenerator} from './CityGenerator';

class MapGenerator {
  private readonly map : GeneratedMap;
  private readonly cityNumber: number;

  constructor(mapWidth : number, mapHeight : number, densityEs : number, densityR : number, cityNumber = 20) {
    this.cityNumber = cityNumber;
    this.map = new GeneratedMap(mapHeight, mapWidth, densityEs, densityR);
  }

  /**
   * Génère une carte avec des villes et des rivières.
   * @param {number} cellWidth La largeur d'une case de la carte.
   * @return {GeneratedMap}
   */
  generate(cellWidth : number) : GeneratedMap {
    const seaLevel = this.map.seaLevel;

    // Génère la carte des hauteurs.
    const cellHeight = ratio(cellWidth);
    const hmg = new HeightMapGenerator(cellWidth, cellHeight);
    const heightMap = hmg.generate(this.map.width, this.map.height);

    // Génère des rivières.
    this.map.grid.cells = heightMap.cells;
    this.map.grid.cellWidth = cellWidth;
    this.map.grid.cellHeight = cellHeight;
    const errorMargin = 0.001 * ((1 - seaLevel) / 0.5)**2;
    const riverGen = new RiverGenerator(heightMap, errorMargin, seaLevel);
    const riversNumber = 100 * Math.max(this.map.height, this.map.width) / 1000 * (4*this.map.riverDensity);
    for (let i = 0; i < riversNumber; ++i) {
      this.map.grid.rivers.push(riverGen.generate((1-seaLevel) * 0.2 + seaLevel));
    }

    // Génère des villes.
    const cityGen = new CityGenerator(heightMap, seaLevel);
    this.map.grid.cities = cityGen.generate(this.cityNumber);
    return this.map;
  }
}

export default MapGenerator;
