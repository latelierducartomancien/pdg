import {CityGenerator} from './CityGenerator';
import {HeightMapGenerator} from './HeightMapGenerator';

describe('CityGenerator.ts', () => {
  describe('CityGenerator', () => {
    it('01 - generate right number', () => {
      for (let n = 0; n <= 10; ++n) {
        const gen = new HeightMapGenerator(10, 10);
        const grid = gen.generate(300, 300);
        const cityNumber = 10;
        const cityGen = new CityGenerator(grid, n/10);
        const cities = cityGen.generate(cityNumber);
        expect(cities.length).toStrictEqual(cityNumber);
      }
    });
  });
});
