import {Vector2D} from '../Utils/Vector';
import {Cell} from '../model/Cell';
import {HeightMap} from '../model/HeightMap';
import {River} from '../model/River';
import {HexCoord} from '../Utils/HexagonalCoordinate';

export class RiverGenerator {
  private highMap: HeightMap;
  private readonly toleranceMargin: number;
  private readonly seaLevel: number;

  constructor(heightMap: HeightMap, errorMargin = 0.005, seaLevel = 0.54) {
    this.highMap = heightMap;
    this.toleranceMargin = errorMargin;
    this.seaLevel = seaLevel;
  }

  /**
   * Génère une rivière à partir de la hauteur donnée.
   * @param {number} riverMinStartLevel La hauteur minimum du début de la rivière.
   * @return {River}
   */
  generate(riverMinStartLevel = this.seaLevel): River {
    riverMinStartLevel = Math.max(this.seaLevel, riverMinStartLevel);
    const mapSize = this.highMap.xNum * this.highMap.yNum;
    let currCellInfo: CellInfo;
    const river = new River();
    // Choix de la cellule de départ.
    let count = 0;
    do {
      const xRand = Math.floor(Math.random() * this.highMap.xNum);
      const yRand = Math.floor(Math.random() * this.highMap.yNum);
      const currCellTabCoord = new Vector2D(xRand, yRand);
      const currCell = this.highMap.cells[xRand][yRand];
      currCellInfo = new CellInfo(currCell, currCellTabCoord);
      ++count;
    } while ((currCellInfo.cell.level < riverMinStartLevel ||
      this.cellHasRiver(currCellInfo.coord)) &&
    count <= this.seaLevel * mapSize);
    if (count >= this.seaLevel * mapSize) {
      return river;
    }
    const riverInit = currCellInfo.coord;
    this.markCellWithRiver(currCellInfo.coord);
    river.riverPoints.push(riverInit);
    // Construction de la rivière.
    let neighbors: CellInfo[];
    do {
      neighbors = this.findNeighbors(currCellInfo);
      if (neighbors.length == 0) break; // Si aucune case voisine trouvée, on quitte.
      // Choisi une case voisine parmi la liste de voisins sur laquelle la rivière n'est pas déjà passé.
      let search: boolean;
      let tmpNeighbor: CellInfo;
      // Choisi une case voisin avec les bonnes propriétés.
      do {
        search = false;
        do {
          // Choisi une case voisin au hasard.
          const rand = Math.floor(Math.random() * neighbors.length);
          tmpNeighbor = neighbors[rand];
          // Supprime la case si elle se trouve dans rivière courante...
          if (tmpNeighbor.marked) neighbors.splice(rand, 1);
          // Et recommence
        } while (tmpNeighbor.marked && neighbors.length > 0);
        if (neighbors.length === 0) break; // S'il n'a plus de case voisine, on quitte.
        // Marque la case voisine si elle était déjà dans la rivière.
        // Chercher d'abord dans les dernières cases ajoutées.
        for (let i = river.riverPoints.length - 1; i >= 0; --i) {
          if (river.riverPoints[i].equals(tmpNeighbor.coord)) {
            search = true;
            tmpNeighbor.marked = true;
            break;
          }
        }
      } while (search);
      // Vérifier si la case voisine était dans une rivière précédente.
      if (this.cellHasRiver(tmpNeighbor.coord)) {
        // Si oui, alors on rejoint l'autre rivière et on s'arrête là.
        neighbors = [];
      }
      this.markCellWithRiver(tmpNeighbor.coord);
      river.riverPoints.push(tmpNeighbor.coord);

      // Actualise la case courante pour le prochain tour.
      currCellInfo = tmpNeighbor;
      currCellInfo.marked = true;
    } while (neighbors.length > 0 && river.riverPoints.length < mapSize);
    return river;
  }

  /**
   * Indique qu'une rivière passe par cette case.
   * @param {Vector2D} coord Les coordonnées de la case.
   */
  markCellWithRiver(coord: Vector2D) {
    this.highMap.cells[coord.x][coord.y].hasRiver = true;
  }

  /**
   * Permet de savoir si une rivière passe par cette case.
   * @param {Vector2D} coord Les coordonnées de la case.
   * @return {boolean}
   */
  cellHasRiver(coord: Vector2D): boolean {
    return this.highMap.cells[coord.x][coord.y].hasRiver;
  }

  /**
   * Trouve les cases voisines à une case données.
   * @param {number} cellInfo La case dont on souhaite trouver les cases voisines.
   * @return {CellInfo[]}
   */
  findNeighbors(cellInfo: CellInfo): CellInfo[] {
    const neighbors = [];
    // Cherche les cellules voisines et récupère leurs hauteurs.
    const hexaCoord = HexCoord.cartesianToHexCoord(cellInfo.coord);
    for (let i = 0; i < 6; ++i) {
      const coord = hexaCoord.neighbor(i);
      const neighbor =
        this.highMap.getCellAtHexCoord(new Vector2D(coord.q, coord.r));
      if (neighbor !== undefined && (neighbor.level - cellInfo.cell.level) < this.toleranceMargin &&
        (this.seaLevel - neighbor.level) < this.toleranceMargin) {
        const tmpCoord = HeightMap.hexToCartesianCoord(new Vector2D(coord.q, coord.r));
        neighbors.push(new CellInfo(neighbor, tmpCoord, false));
      }
    }
    return neighbors;
  }
}

class CellInfo {
  public cell: Cell;
  public coord: Vector2D;
  public marked: boolean;

  constructor(cell: Cell, coord: Vector2D, marked = false) {
    this.cell = cell;
    this.coord = coord;
    this.marked = marked;
  }
}
