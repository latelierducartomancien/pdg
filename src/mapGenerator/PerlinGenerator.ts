import {Vector2D} from '../Utils/Vector';

export class PerlinGenerator {
  gradients: Vector2D[][];
  private dim: number;

  constructor(dim = 4) {
    this.dim = dim;
    // liste des gradients
    this.gradients = [];
    for (let i = 0; i < dim; ++i) {
      this.gradients[i] = [];
      for (let j = 0; j < dim; ++j) {
        this.gradients[i][j] = Vector2D.random();
      }
    }
  }

  /*
   * Calcule le produit scalaire sur la grille.
   */
  private dot_prod_grid(x: number, y: number, vx: number, vy: number): number {
    const dist = new Vector2D(x - vx, y - vy);
    if (vx >= this.gradients.length || vy >= this.gradients[vx].length) {
      return 0;
    } else {
      const gVect = this.gradients[vx][vy];
      return dist.dotProd(gVect);
    }
  }

  /**
   * Fonction définissant la courbe pour le lissage de l'interpolation.
   * @param {number} x La valeur a interpoler.
   * @return {number} La valeur interpolée.
   */
  private static smootherstep(x: number): number {
    return -2 * x**3 + 3 * x**2;
  }

  /**
   * Interpole un nombre entre deux bornes.
   * @param {number} x La valeur a interpoler.
   * @param {number} min La borne minimale.
   * @param {number} max La borne maximale.
   * @return {number} La valeur interpolée.
   */
  private static interpolate(x: number, min: number, max: number) : number {
    return min + PerlinGenerator.smootherstep(x) * (max - min);
  }

  /**
   * Génère la hauteur aux coordonnées donnes.
   * @param {number} x La coordonnée horizontale.
   * @param {number} y La coordonnée verticale.
   * @return {number} La hauteur générée.
   */
  generateHeight(x: number, y: number) : number {
    const intX = Math.floor(x);
    const intY = Math.floor(y);
    // interpolation
    // top
    const topL = this.dot_prod_grid(x, y, intX, intY);
    const topR = this.dot_prod_grid(x, y, intX + 1, intY);
    const xTop = PerlinGenerator.interpolate(x - intX, topL, topR);
    // bottom
    const botL = this.dot_prod_grid(x, y, intX, intY + 1);
    const botR = this.dot_prod_grid(x, y, intX + 1, intY + 1);
    const xBot = PerlinGenerator.interpolate(x - intX, botL, botR);
    // final interpolation
    const v = PerlinGenerator.interpolate(y - intY, xTop, xBot);
    return v;
  }
}
