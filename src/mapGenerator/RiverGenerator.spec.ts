import {RiverGenerator} from './RiverGenerator';
import {HeightMapGenerator} from './HeightMapGenerator';

describe('RiverGenerator.ts', () => {
  describe('RiverGenerator', () => {
    it('01 - generateRivers', () => {
      for (let n = 0; n < 100; ++n) {
        const gen = new HeightMapGenerator(10, 10);
        const grid = gen.generate(300, 300);
        const errorMargin = 0.005;
        const riverGen = new RiverGenerator(grid, errorMargin, 0);
        const river = riverGen.generate();
        let lp = river.riverPoints[0];
        for (let j = 1; j < river.riverPoints.length; ++j) {
          const cp = river.riverPoints[j];
          const lastCellLvl = grid.cells[lp.x][lp.y].level;
          const currCellLvl = grid.cells[cp.x][cp.y].level;
          expect(currCellLvl - lastCellLvl).toBeLessThanOrEqual(errorMargin);
          lp = cp;
        }
      }
    });
    it('02 - start at right lvl', () => {
      const gen = new HeightMapGenerator(10, 10);
      const grid = gen.generate(300, 300);
      const errorMargin = 0.005;
      const riverGen = new RiverGenerator(grid, errorMargin, 0.5);
      const river = riverGen.generate(0.8);
      const coord = river.riverPoints[0];
      const cell = grid.cells[coord.x][coord.y];
      expect(cell.level).toBeGreaterThanOrEqual(0.8);
    });
    it('03 - no infinite loop', () => {
      const gen = new HeightMapGenerator(10, 10);
      const grid = gen.generate(300, 300);
      const errorMargin = 0.005;
      const riverGen = new RiverGenerator(grid, errorMargin, 0.5);
      for (let n = 0; n < 100; ++n) {
        const river = riverGen.generate();
        expect(river.riverPoints.length).toBeLessThan(grid.cells.length);
      }
    });
  });
});
