import * as Chance from 'chance';
import {City} from '../model/City';
import {HeightMap} from '../model/HeightMap';
import {Cell} from '../model/Cell';

const chance: Chance = new Chance();

export class CityGenerator {
  private heightMap: HeightMap;
  private readonly seaLevel: number;

  constructor(heightMap: HeightMap, seaLevel = 0.54) {
    this.heightMap = heightMap;
    this.seaLevel = seaLevel;
  }

  /**
   * Génère un nombre de villes donné sur la carte.
   * @param {number} n Le nombre de villes à générer.
   * @return {City[]}
   */
  generate(n: number): City[] {
    const mapSize = this.heightMap.xNum * this.heightMap.yNum;
    // Génère les villes.
    const cities = [];
    for (let i = 0; i < n; ++i) {
      let count = 0;
      let currCell: Cell;
      do {
        const xRand = Math.floor(Math.random() * this.heightMap.xNum);
        const yRand = Math.floor(Math.random() * this.heightMap.yNum);
        currCell = this.heightMap.cells[xRand][yRand];
        ++count;
      } while (currCell.level <= this.seaLevel && count <= this.seaLevel * mapSize && !currCell.hasRiver);
      cities.push(new City(chance.city(), currCell.position));
    }
    return cities;
  }
}
