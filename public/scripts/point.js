/**
 * Calcule un point de la médiatrice entre deux points à une distance aléatoire.
 * @param p1 Le premier point de la droite.
 * @param p2 Le second point de la droite.
 * @return {{x: number, y: number}} Le point calculé.
 */
export function randomShift(p1, p2) {
  return shift(p1, p2, -dist(p1, p2) / 3 * (Math.random() - 0.5) * 2);
}

/**
 * Calcule un point de la médiatrice entre deux points à une distance donnée.
 * @param p1 Le premier point de la droite.
 * @param p2 Le second point de la droite.
 * @param n La distance entre la droite et le point calculé.
 * @return {{x: number, y: number}} Le point calculé.
 */
function shift(p1, p2, n = 10) {
  const pm = midPoint(p1, p2);
  const pp = perpendicular(p1, p2);
  return {x: pm.x + pp.x * n, y: pm.y + pp.y * n};
}

/**
 * Calcule le point milieu entre deux points.
 * @param p1 Le premier point.
 * @param p2 Le second point.
 * @return {{x: number, y: number}} Le point milieu.
 */
function midPoint(p1, p2) {
  return {x: (p2.x + p1.x) / 2, y: (p2.y + p1.y) / 2};
}

function perpendicular(p1, p2) {
  const pp = diff(p1, p2);
  return {x: ((pp.y) / norm(pp)), y: ((-pp.x) / norm(pp))};
}

/**
 * Calcule la position relative d'un point à un autre point.
 * @param p1 Le point à partir duquel calculer la position relative.
 * @param p2 Le point dont on souhaite savoir la position relative.
 * @return {{x: number, y: number}} La position relative.
 */
function diff(p1, p2) {
  return {x: p2.x - p1.x, y: p2.y - p1.y};
}

/**
 * Calcule la norme d'un point par rapport à l'origine.
 * @param p Le point dont on souhaite savoir la norme.
 * @return {number} La norme du point.
 */
function norm(p) {
  return Math.hypot(p.x, p.y);
}

/**
 * Calcule la distance entre deux points.
 * @param p1 Le premier point.
 * @param p2 Le second point.
 * @return {number} La distance entre les deux points.
 */
function dist(p1, p2) {
  return norm(diff(p1, p2));
}
