document.addEventListener('DOMContentLoaded', () => {
  const regenerate = document.querySelector('#regenerate');
  const background = document.querySelector('.regenerate-background');
  const abortButton = background.querySelector('.regenerate-abort');
  const confimButton = background.querySelector('.regenerate-confirm');

  regenerate.addEventListener('submit', (event) => {
    // Ne navigue pas jusqu'à la page des réglagles.
    event.preventDefault();
    // A la place, on affiche le modal demandant la confirmation de l'utilisateur.
    background.style.display = 'block';
  });

  // Si l'utilisateur a changé d'avis, on ferme le modal et reste sur la page.
  abortButton.addEventListener('click', () => {
    background.style.display = 'none';
  });

  // Si l'utilisateur a confirmé son choix, on déclanche la navigation jusqu'à la page.
  confimButton.addEventListener('click', () => {
    background.style.display = 'none';
    regenerate.submit();
  });
});