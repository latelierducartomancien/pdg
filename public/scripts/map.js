import {newSep, switchColorOnLevel} from './interpolation.js';
import {randomShift} from './point.js';

// Le nombre de côtés d'un hexagone.
const SIDES_PER_HEXAGON = 6;
const MAX_SEA_LEVEL_INDEX = 2;
let SAVED_MAP = null;

/**
 * Affiche le nom des villes dans une liste sur la page.
 * @param {GeneratedMap} map La carte dont on souhaite afficher les villes.
 */
function loadCityNames(map) {
  const cityList = document.getElementById('cityList');

  for (let i = 0; i < map.grid.cities.length; ++i) {
    const city = map.grid.cities[i];
    const item = document.createElement('li');
    const input = document.createElement('input');
    input.maxLength = 10;
    input.setAttribute('data-city-id', `${i}`);
    input.value = city.name;
    item.appendChild(input);
    cityList.appendChild(item);
  }

  cityList.addEventListener('change', (event) => {
    const input = event.target;
    const id = input.getAttribute('data-city-id');
    const city = map.grid.cities[id];
    city.name = input.value;

    // @type HTMLCanvasElement
    const canvas = document.querySelector('#canvas');
    // @type CanvasRenderingContext2D
    const ctx = canvas.getContext('2d');
    ctx.putImageData(SAVED_MAP, 0, 0);
    for (const city of map.grid.cities) {
      displayCity(ctx, city, canvas.width, canvas.height);
    }
  });
}

/**
 * Affiche la carte sur la page.
 * @param {GeneratedMap} map La carte à afficher.
 */
export function* displayMap(map) {
  const downloadbtn = document.getElementById('downloadBtn');
  downloadbtn.disabled = true;
  const canvas = document.querySelector('#canvas');
  canvas.height = map.height;
  canvas.width = map.width;
  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const ref = [0.3, 0.45, 0.55, 0.59, 0.7, 0.82, 0.85, 0.9];
  let levSep = new Array(ref.length);
  if (Math.abs(map.seaLevel - ref[MAX_SEA_LEVEL_INDEX]) > 0.01) {
    for (let i = 0; i < ref.length; ++i) {
      levSep[i] = newSep(ref[i], ref[MAX_SEA_LEVEL_INDEX], map.seaLevel);
    }
  } else {
    levSep = ref;
  }

  for (const l of map.grid.cells) {
    for (const cell of l) {
      displayCell(ctx, cell, map.grid.cellWidth,
          levSep);
    }
    yield;
  }

  for (const river of map.grid.rivers) {
    displayRiver(ctx, river.riverPoints, map.grid.cells, map.grid.cellWidth);
    yield;
  }
  SAVED_MAP = ctx.getImageData(0, 0, canvas.width, canvas.height);

  for (const city of map.grid.cities) {
    displayCity(ctx, city, canvas.width, canvas.height);
    yield;
  }

  // change visibility map
  const waiting = document.querySelector('#waitingMap');
  waiting.style.display = 'none';

  downloadbtn.disabled = false;
}

/**
 * Affiche une ville donnée sur la carte.
 * @param {CanvasRenderingContext2D} ctx Le contexte utilisé pour afficher la ville.
 * @param {City} city La ville à afficher.
 * @param {number} cW La largeur de la ville.
 * @param {number} cH La hauteur de la ville.
 */
function displayCity(ctx, city, cW, cH) {
  // Largeur maximum du nom d'une ville.
  const maxNameWidth = 110;
  // Hauteur maximum du nom d'une ville.
  const maxNameHeight = 25;
  // Largeur du rectangle représentant la ville.
  const cityWidth = 4;
  // Hauteur du rectangle représentant la ville.
  const cityHeight = 4;
  ctx.font = '25px TimesNewRoman';
  ctx.fillStyle = 'rgb(0,0,0)';
  ctx.textAlign = 'center';
  ctx.fillRect(city.coords.x-cityWidth/2, city.coords.y-cityHeight/2, cityWidth, cityHeight);
  let posX = city.coords.x;
  let posY = city.coords.y;
  if (city.coords.x - maxNameWidth/2 < 0) {
    posX += maxNameWidth/2;
  }
  if (city.coords.x + maxNameWidth/2 > cW) {
    posX -= maxNameWidth/2;
  }
  if (city.coords.y - maxNameHeight/2 < 0) {
    posY += maxNameHeight;
  }
  if (city.coords.y + maxNameHeight/2 > cH) {
    posY -= maxNameHeight;
  }
  ctx.fillText(city.name, posX, posY - cityHeight);
}

/**
 * Affiche une rivière donnée sur la carte.
 * @param {CanvasRenderingContext2D} ctx Le contexte utilisé pour afficher la ville.
 * @param {Vector2D[]} riverPoints Les points par laquelle la rivière passe.
 * @param {Cell[][]} cells Les cases de la carte.
 * @param {number} cellWidth La largeur d'une case.
 */
function displayRiver(ctx, riverPoints, cells, cellWidth) {
  for (let i = 1; i < riverPoints.length; ++i) {
    const rp0 = riverPoints[i - 1];
    const start = cells[rp0.x][rp0.y].position;
    const rp1 = riverPoints[i];
    const end = cells[rp1.x][rp1.y].position;
    ctx.beginPath();
    ctx.moveTo(start.x, start.y);
    const mid = randomShift(start, end);
    ctx.lineTo(mid.x, mid.y);
    ctx.lineTo(end.x, end.y);
    ctx.strokeStyle = 'rgb(0,117,124)';
    ctx.lineWidth = 1 + Math.min(cellWidth, i / 8);
    ctx.stroke();
  }
}

/**
 * Affiche une case de la carte.
 * @param {CanvasRenderingContext2D} ctx Le contexte utilisé pour afficher la case.
 * @param {Cell} cell La case à afficher.
 * @param {number} cellWidth La largeur d'une case.
 * @param {[][number,number,number]} levSep Tableau des différents niveaux de hauteur.
 */
function displayCell(ctx, cell, cellWidth, levSep) {
  const size = cellWidth / Math.sqrt(3);
  const sides = hexagonSides(cell.position, size);

  ctx.beginPath();
  const start = sides[0];
  ctx.moveTo(start.x, start.y);
  for (let i = 1; i < sides.length; ++i) {
    const dest = sides[i];
    ctx.lineTo(dest.x, dest.y);
  }
  ctx.closePath();

  let c = switchColorOnLevel(
      levSep,
      [[11, 54, 80], [11, 77, 100], [0, 117, 124], [208, 192, 154], [107, 185, 43], [52, 100, 42], [121, 120, 117], [156, 157, 157], [255, 255, 255]],
      cell.level,
  );
  c = randomizeColor(c, 0.05);
  ctx.fillStyle = getColor(c);
  ctx.strokeStyle = getColor(c);
  ctx.fill();
  ctx.stroke();
}

/**
 * Modifie aléatoirement une couleur.
 * @param {number[]} c La couleur initiale.
 * @param {number} randomRatio Le ratio de randomisation.
 * @return {number[]} La nouvelle couleur modifiée.
 */
function randomizeColor(c, randomRatio) {
  const r = [Math.random(), Math.random(), Math.random()];
  return [
    (c[0] * (1 + r[0] * randomRatio)),
    (c[1] * (1 + r[1] * randomRatio)),
    (c[2] * (1 + r[2] * randomRatio)),
  ];
}

/**
 * Converti une couleur au format CSS.
 * @param {[number, number, number]} c La couleur à convertir.
 * @return {string} La couleur convertie.
 */
function getColor(c) {
  return `rgb(${c[0]},${c[1]},${c[2]})`;
}

/**
 * Les coordonnées des côtés d'un hexagone.
 * @param {Vector2D} center Le centre de l'hexagone.
 * @param {number} size La taille de l'hexagone.
 * @return {Vector2D[]} Les côtés de l'hexagone.
 */
function hexagonSides(center, size) {
  const sides = new Array(SIDES_PER_HEXAGON);
  for (let i = 0; i < sides.length; ++i) {
    const angle = 60 * i - 30;
    const rad = Math.PI / 180 * angle;

    sides[i] = {
      x: center.x + size * Math.cos(rad),
      y: center.y + size * Math.sin(rad),
    };
  }
  return sides;
}

function nextTick() {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, 0);
  });
}

/**
 * Fonction principale affichant la carte.
 */
async function main() {
  const response = await fetch('/map/generate');
  const map = await response.json();
  for (const cell of displayMap(map)) {
    await nextTick();
  }
  loadCityNames(map);
}

document.addEventListener('DOMContentLoaded', () => main());
