/**
 * Courbe pour le lissage dans l'interpolation.
 * @param {number} x La valeur à interpoler.
 * @return {number} La valeur interpolée.
 */
function smootherstep(x) {
  return (-2 * x ** 3 + 3 * x ** 2);
}

/**
 * Interpole une nombre entre deux bornes.
 * @param {number} x La valeur à interpoler.
 * @param {number} a La borne minimum.
 * @param {number} b La borne maximum.
 * @return {number} La valeur interpolée.
 */
function interp(x, a, b) {
  return a + smootherstep(x) * (b - a);
}

/**
 * Interpole une couleur entre deux couleurs données.
 * @param {number} l Le niveau d'interpolation.
 * @param {[number, number, number]} c1 La couleur de départ.
 * @param {[number, number, number]} c2 La couleur d'arrivée.
 * @return {[number, number, number]} La couleur interpolée.
 */
function interp3(l, c1, c2) {
  return [
    interp(l, c1[0], c2[0]),
    interp(l, c1[1], c2[1]),
    interp(l, c1[2], c2[2]),
  ];
}

/**
 * Récupère la couleur correspondant au niveau de hauteur.
 * @param {number[]} lSep La liste des séparateurs (n-1)
 * @param {[number, number, number][]} colors La liste des couleurs (n)
 * @param {number} level Le niveau de hauteur actuel.
 * @return {[number, number, number]} La couleur correspondant au niveau.
 */
export function switchColorOnLevel(lSep, colors, level) {
  let color;
  if (level <= lSep[0]) return colors[0];
  for (let i = 1; i < lSep.length; ++i) {
    if (level < lSep[i]) {
      color = interp3((level - lSep[i - 1]) / (lSep[i] - lSep[i - 1]), colors[i - 1], colors[i]);
      break;
    } else {
      color = interp3((level - lSep[i]) / (1 - lSep[i]), colors[i], colors[i + 1]);
    }
  }
  return color;
}

/**
 * Adapte la valeur d'un séparateur en fonction du niveau de la mer.
 * @param {number} oldSep Le séparateur initial.
 * @param {number} defaultSeeLvl La hauteur maximale du niveau de la mer.
 * @param {number} newSeeLvl Le niveau de la mer choisi.
 * @return {number} La valeur adaptée du séparateur.
 */
export function newSep(oldSep, defaultSeeLvl, newSeeLvl) {
  if (oldSep <= defaultSeeLvl) {
    return (newSeeLvl) * (oldSep / defaultSeeLvl);
  } else {
    return (1 - newSeeLvl) * ((oldSep - defaultSeeLvl) / (1 - defaultSeeLvl)) + newSeeLvl;
  }
}
