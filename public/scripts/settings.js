const terreMerSlider = document.getElementById('terre-mer-slider');
const terreMerOutput = document.getElementById('terre-mer-value');
terreMerOutput.textContent = `${terreMerSlider.value} %`;

// Met à jour l'affichage de la valeur actuelle du slider (à chaque fois que le slider est déplacé)
terreMerSlider.addEventListener('input', () => {
  terreMerOutput.textContent = `${terreMerSlider.value} %`;
});

const riverSlider = document.getElementById('river-slider');
const riverOutput = document.getElementById('river-value');
riverOutput.textContent = `${riverSlider.value} %`;

// Met à jour l'affichage de la valeur actuelle du slider (à chaque fois que le slider est déplacé)
riverSlider.addEventListener('input', () => {
  riverOutput.textContent = `${riverSlider.value} %`;
});
